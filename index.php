<?php
include "include/db.php";

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="index.css" rel="stylesheet">
    <title>Document</title>
</head>

<body>

    <?php
    include "nav.php";
    ?>

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">

            <?php
            for ($x = 0; $x < count($ponies); $x++) { ?>
                <div class="carousel-item <?php if ($x == 0) {
                                                echo 'active';
                                            } ?>">
                    <img src="<?php echo "images/" . $ponies[$x]['picture']; ?> " class="d-block w-100" alt="...">
                </div>
            <?php } ?>

            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>


    <div class="container">
        <div class="card mb-3" style="max-width:100%;">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="images/etable.jpg" class="card-img" alt="...">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h1 class="card-title">Poney-Club-sur-le-Loir</h1>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In volutpat, risus et
                            vehicula aliquam, justo sapien accumsan urna, nec dictum urna velit vitae risus.
                            Vestibulum congue, mi sit amet suscipit interdum, lacus dui eleifend felis, vitae
                            rhoncus dolor ligula iaculis nisl. Nullam quis felis sodales, congue lacus sed,
                            vestibulum diam. Phasellus vehicula tincidunt mollis. In suscipit dignissim diam, id
                            accumsan tellus porta vitae.
                            Donec sem ante, tempor at tortor id, lobortis congue elit. Donec ut dolor lectus.
                            Vestibulum dapibus, est at suscipit aliquet, turpis velit fermentum quam, id
                            feugiat est tortor et justo. In sed sem vitae ante tempor tristique. Fusce odio est,
                            tempus a nisl eget, cursus bibendum felis. Maecenas semper dui id sem varius
                            pellentesque. Vestibulum dictum augue ac ligula tempus, non dapibus sem
                            pulvinar.
                            In a fermentum sapien. Phasellus tellus tellus, elementum a sollicitudin vitae,
                            vestibulum eget ante. Vivamus sollicitudin tellus vel urna condimentum, eu
                            tempus risus tincidunt. Donec porta tristique metus, a semper quam porta eu.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    include "footer.php";

    ?>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>