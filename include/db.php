<?php

/*
 * Ce tableau provient d'une base de données contenant la liste des poneys du club.
 */

$ponies = [
    [
        'name' => 'Tequila',
        'picture' => 'shetland-pony-PKBJV7G.jpg',
        'age' => 2,
    ],
    [
        'name' => 'Sushi',
        'picture' => 'shetland-pony-P9S7437.jpg',
        'age' => 3,
    ],
    [
        'name' => 'Aly',
        'picture' => 'shetland-pony-2-years-PTFY8RA.jpg',
        'age' => 4,
    ],
    [
        'name' => 'Chenapan',
        'picture' => 'shetland-pony-2-years-PM2HC8J.jpg',
        'age' => 1,
    ],
    [
        'name' => 'Lou',
        'picture' => 'shetland-pony-2-years-PHVHW9L.jpg',
        'age' => 2,
    ],
];
