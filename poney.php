<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="index.css" rel="stylesheet">
    <title>Document</title>
</head>

<body>
    <?php
    include "nav.php";
    include "include/db.php";
    ?>
    <div class="container">
        <div class="row">

            <?php
            foreach ($ponies as $cle => $valeur) {

                echo '<div class="col-lg-4 mt-4" >
             <div class="card" style="width: 18rem;">
                        <img src=" images/' . $valeur['picture'] . '" class="card-img-top" alt="...">
        
                            <div class="card-body">

            <h5 class="card-title" > ' . $valeur['name'] . ' </h5>
<p class="card-text">' . $valeur['age'] . ' an' . ($valeur['age'] > 1 ? "s" : "") . '</p></div>
        </div>
    </div>';
            }
            ?>
        </div>
    </div>

    <?php
    include "footer.php";

    ?>
    
</body>

</html>